import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  List,
  Input,
  Checkbox,
  ListItem,
  ListItemIcon,
  ListItemSecondaryAction,
  ListItemText,
  IconButton,
  withStyles,
} from '@material-ui/core';
import DoneIcon from '@material-ui/icons/Done';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  child: {
    borderRadius: 5,
    backgroundColor: '#fff',
    marginBottom: '6px',
    height: 70,
    boxShadow:
      '0px 5px 5px -3px rgb(0 0 0 / 20%), 0px 8px 10px 1px rgb(0 0 0 / 14%), 0px 3px 14px 2px rgb(0 0 0 / 12%)',
  },
  verticleLine: {
    height: '69px',
    borderLeft: '4px solid #13C2DB',
    position: 'absolute',
    left: 0,
    top: 1,
  },
}));

const WhiteTextField = withStyles({
  root: {
    '&.MuiInput-underline:before': {
      borderBottomColor: '#13C2DB', // Semi-transparent underline
    },
    '&.MuiInput-underline:hover:before': {
      borderBottomColor: '#13C2DB', // Solid underline on hover
    },
    '&.MuiInput-underline:after': {
      borderBottomColor: '#13C2DB', // Solid underline on focus
    },
  },
})(Input);

interface myProps {
  tasks: {
    id: number;
    title: string;
    status: string;
  }[];
  completed: boolean;
  onEdit: (id: number, title: string) => void;
  onDelete: (id: number) => void;
}

export const CheckboxList: React.FC<myProps> = ({ tasks, onEdit, onDelete, completed }) => {
  const classes = useStyles();
  const [title, setTitle] = React.useState('');
  const [checked, setChecked] = React.useState([0]);
  const [editStatus, setEditStatus] = React.useState(-1);

  React.useEffect(() => {
    let status = [0];
    tasks.map((task) => {
      if (task.status === 'Done') status.push(task.id);
    });
    setChecked(status);
    console.log(status);
  }, [tasks]);

  const handleToggle = (value: any) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  const onEditClick = (id: number) => {
    setEditStatus(-1);
    onEdit(id, title);
  };

  return (
    <List className={classes.root}>
      {tasks.map((value: any) => {
        const labelId = `checkbox-list-label-${value.id}`;

        return (
          <ListItem
            dense
            button
            key={value.id}
            role={undefined}
            className={classes.child}
            onClick={handleToggle(value.id)}>
            {/* {(completed && value.status === 'Done') ?   */}
            {editStatus === value.id ? (
              <React.Fragment>
                <WhiteTextField value={title} onChange={(e) => setTitle(e.target.value)} />
                <ListItemSecondaryAction>
                  <IconButton
                    onClick={() => onEditClick(value.id)}
                    edge='end'
                    aria-label='comments'>
                    <DoneIcon />
                  </IconButton>
                </ListItemSecondaryAction>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <ListItemIcon>
                  <>
                    <div className={classes.verticleLine} />
                    <Checkbox
                      edge='start'
                      checked={checked.indexOf(value.id) !== -1}
                      tabIndex={-1}
                      disableRipple
                      color='secondary'
                      key={value.id}
                      inputProps={{ 'aria-labelledby': labelId }}
                    />
                  </>
                </ListItemIcon>
                <ListItemText id={labelId} primary={`${value.title}`} />
                <ListItemSecondaryAction>
                  <IconButton
                    onClick={() => {
                      setEditStatus(value.id);
                      setTitle(value.title);
                    }}
                    edge='end'
                    aria-label='comments'>
                    <EditIcon />
                  </IconButton>
                  <IconButton onClick={() => onDelete(value.id)} edge='end' aria-label='comments'>
                    <DeleteIcon />
                  </IconButton>
                </ListItemSecondaryAction>
              </React.Fragment>
            )}
          </ListItem>
        );
      })}
    </List>
  );
};
