import axios, { AxiosResponse } from 'axios';

const api = axios.create({
  baseURL: 'http://localhost:3000',
  headers: {
    'Content-Type': 'application/json',
  },
});

export const setAuthToken = async (token: string) => {
  try {
    api.defaults.headers.common.Authorization = 'bearer ' + token;
  } catch (e) {
    console.log({ e });
  }
};

api.interceptors.response.use(
  (res: AxiosResponse) => res.data,
  (error) => {
    console.log(error.response.data);
    return Promise.reject(error.response.data);
  }
);

export default api;
