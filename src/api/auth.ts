import api from './api';

export const authApi = {
  login: (body: any) => api.post(`/login`, { ...body }),

  register: (body: any) => api.post(`/register`, { ...body }),
};
