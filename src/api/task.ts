import api from './api';
const PATH = 'task';

export const taskApi = {
  getTasks: () => api.get(`/${PATH}`),

  addTask: (id: number, body: any) => api.post(`/${PATH}`, { checklistId: id, title: body }),

  updateTask: (id: number, title: string) => api.patch(`${PATH}/${id}`, { title }),

  deleteTask: (id: number) => api.delete(`${PATH}/${id}`),
};
