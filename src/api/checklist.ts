import api from './api';
const PATH = 'checklist';

export const checklistApi = {
  addChecklist: (body: any) => api.post(`/${PATH}`, { title: body }),

  getChecklists: (body: any) => api.get(`/${PATH}`),

  getChecklistTasks: (id: number) => api.get(`/${PATH}/${id}`),
};
