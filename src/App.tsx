import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Login, Register, Home } from './pages';

const Routes = () => (
  <Switch>
    <Route exact path='/' component={Home} />
    <Route exact path='/login' component={Login} />
    <Route exact path='/Register' component={Register} />
  </Switch>
);

const App: React.FC = () => {
  return (
    <Router>
      <Routes />
    </Router>
  );
};

export default App;
