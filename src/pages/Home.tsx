import * as React from 'react';
import { useHistory } from 'react-router-dom';
import { CheckboxList } from '../components';
import { taskApi, checklistApi, setAuthToken } from '../api';
import { Box, makeStyles, Modal, TextField, Button, Chip } from '@material-ui/core';
import AppBarComponent from './AppBar';
import ChecklistDrawer from './ChecklistDrawer';

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '1px solid white',
    padding: theme.spacing(2, 4, 3),
  },
  input: {
    marginBottom: '20px',
  },
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  chip: {
    color: 'white',
    backgroundColor: '#2A2E4A',
  },
}));

const rand = () => {
  return Math.round(Math.random() * 20) - 10;
};

const getModalStyle = () => {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
};

export const Home = () => {
  const classes = useStyles();
  const history: any = useHistory();
  const [task, setTask] = React.useState('');
  const [title, setTitle] = React.useState('');
  const [status, setStatus] = React.useState(0);
  const [tasks, setTasks] = React.useState<any>([]);
  const [modalStyle] = React.useState(getModalStyle);
  const [checklist, setChecklist] = React.useState(0);
  const [checklists, setChecklists] = React.useState([]);
  const [completed, setCompleted] = React.useState(false);

  React.useEffect(() => {
    const token = localStorage.getItem('accessToken');
    if (!token) history.push('/login');
  });

  React.useEffect(() => {
    const getChecklists = async () => {
      try {
        const token: any = localStorage.getItem('accessToken');
        setAuthToken(token);
        const { data }: any = await checklistApi.getChecklists({});
        setChecklists(data.checklists);
      } catch (e) {
        console.log(e.message);
      }
    };

    getChecklists();
  }, []);

  const onSubmit = async () => {
    try {
      const { data } = await checklistApi.addChecklist(title);
    } catch (e) {
      console.log(e.message);
    }
  };

  const onChecklistSubmit = async (id: number) => {
    try {
      const { data } = await checklistApi.getChecklistTasks(id);
      setChecklist(id);
      setTasks(data.tasks);
    } catch (e) {
      console.log(e.message);
    }
  };

  const addTask = async () => {
    try {
      const { data } = await taskApi.addTask(checklist, task);
      const taskList = tasks;
      taskList.push(data);
      setTasks(taskList);
      setStatus(0);
    } catch (e) {
      console.log(e.message);
    }
  };

  const onEdit = async (id: number, title: string) => {
    const { data } = await taskApi.updateTask(id, title);
    const filteredTasks = tasks.filter((task: any) => task.id !== data.id);
    filteredTasks.push(data);
    setTasks(filteredTasks);
  };
  const onDelete = async (id: number) => {
    await taskApi.deleteTask(id);
    const filteredTasks = tasks.filter((task: any) => task.id !== id);
    setTasks(filteredTasks);
  };

  return (
    <>
      <AppBarComponent />
      <Box height='100%' width='100%' display='flex'>
        <Modal
          open={!status ? false : true}
          onClose={() => setStatus(0)}
          aria-labelledby='simple-modal-title'
          aria-describedby='simple-modal-description'>
          {status === 1 ? (
            <div style={modalStyle} className={classes.paper}>
              <TextField
                className={classes.input}
                fullWidth
                placeholder='New Checklist'
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />
              <Button
                onClick={onSubmit}
                type='submit'
                fullWidth
                variant='contained'
                style={{ backgroundColor: '#2A2E4A', color: 'white' }}>
                Submit
              </Button>
            </div>
          ) : (
            <div style={modalStyle} className={classes.paper}>
              <TextField
                className={classes.input}
                fullWidth
                placeholder='New Task'
                value={task}
                onChange={(e) => setTask(e.target.value)}
              />
              <Button
                onClick={addTask}
                type='submit'
                fullWidth
                variant='contained'
                style={{ backgroundColor: '#2A2E4A', color: 'white' }}>
                Submit
              </Button>
            </div>
          )}
        </Modal>

        <ChecklistDrawer
          setStatus={setStatus}
          checklists={checklists}
          onChecklistSubmit={onChecklistSubmit}
        />
        <Box style={{ margin: '10px', width: '70%' }}>
          {!tasks.length ? (
            <Box
              style={{
                height: '100%',
                display: 'flex',
                alignItems: 'center',
                flexDirection: 'column',
                justifyContent: 'center',
              }}>
              <h1>This checklist has no tasks</h1>
              <Button
                onClick={() => setStatus(2)}
                style={{ backgroundColor: '#2A2E4A', color: 'white', padding: '10px 40px' }}>
                Add New Task
              </Button>
            </Box>
          ) : (
            <Box>
              <Box style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Button
                  variant='contained'
                  className={classes.chip}
                  onClick={() => setCompleted(true)}>
                  Show Completed
                </Button>
              </Box>
              <CheckboxList
                tasks={tasks}
                onEdit={onEdit}
                onDelete={onDelete}
                completed={completed}
              />
              <Button
                onClick={() => setStatus(2)}
                style={{ backgroundColor: '#2A2E4A', color: 'white', padding: '10px 40px' }}>
                Add New Task
              </Button>
            </Box>
          )}
        </Box>
      </Box>
    </>
  );
};

export default Home;
