import * as React from 'react';
import {
  Avatar,
  Button,
  CssBaseline,
  TextField,
  Link,
  Grid,
  Box,
  Typography,
  makeStyles,
  Container,
} from '@material-ui/core';
import { useForm, Controller } from 'react-hook-form';
import { authApi } from '../../api';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Copyright from '../Copyright';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export const Register: React.FC = () => {
  const history = useHistory();
  const [accessToken, setAccessToken] = React.useState('');

  React.useEffect(() => {
    const token = localStorage.getItem('accessToken');
    if (token) history.push('/');
  }, [accessToken, history]);

  const classes = useStyles();
  const { handleSubmit, control } = useForm();
  const onSubmit = async (payload: any) => {
    try {
      const data: any = await authApi.register(payload);
      localStorage.setItem('accessToken', data.accessToken);
      setAccessToken(data.accessToken);
      console.log(data);
    } catch (e) {
      console.log(e.message);
    }
  };

  return (
    <Container component='main' maxWidth='xs'>
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component='h1' variant='h5'>
          Sign up
        </Typography>

        <form onSubmit={handleSubmit(onSubmit)} className={classes.form} noValidate>
          <Controller
            as={TextField}
            variant='outlined'
            margin='normal'
            fullWidth
            id='email'
            label='Email Address'
            name='email'
            autoFocus
            control={control}
            defaultValue=''
          />

          <Controller
            as={TextField}
            variant='outlined'
            margin='normal'
            fullWidth
            id='username'
            label='Username'
            name='username'
            autoFocus
            control={control}
            defaultValue=''
          />

          <Controller
            as={TextField}
            variant='outlined'
            margin='normal'
            fullWidth
            name='password'
            label='Password'
            type='password'
            id='password'
            control={control}
            defaultValue=''
          />

          <Button
            type='submit'
            fullWidth
            variant='contained'
            color='primary'
            className={classes.submit}>
            Sign In
          </Button>

          <Grid container>
            <Grid item>
              <Link href='/login' variant='body2'>
                {'Already have an account? Sign in'}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
};

export default Register;
