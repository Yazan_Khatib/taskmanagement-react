import * as React from 'react';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { Box, IconButton, Button } from '@material-ui/core';

interface checklistInterface {
  checklists: {
    id: number;
    title: string;
  }[];
  setStatus: (id: number) => void;
  onChecklistSubmit: (id: number) => {};
}

const ChecklistDrawer: React.FC<checklistInterface> = ({
  setStatus,
  checklists,
  onChecklistSubmit,
}) => {
  return (
    <Box
      style={{
        width: '30%',
        height: '100%',
        backgroundColor: '#2A2E4A',
      }}>
      <Box
        style={{
          color: 'white',
          display: 'flex',
          margin: '0 20px',
          justifyContent: 'space-between',
        }}>
        <h1>CheckLists</h1>
        <IconButton
          onClick={() => setStatus(1)}
          color='secondary'
          aria-label='upload picture'
          component='span'>
          <AddCircleIcon />
        </IconButton>
      </Box>
      <Box>
        {checklists?.map((list: any) => (
          <Button
            key={list.id}
            onClick={() => onChecklistSubmit(list?.id)}
            style={{ width: '100%', color: 'white' }}>
            {list?.title} Checklist
          </Button>
        ))}
      </Box>
    </Box>
  );
};
export default ChecklistDrawer;
