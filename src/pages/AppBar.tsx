import * as React from 'react';
import { useHistory } from 'react-router-dom';
import { Toolbar, Typography, Button, AppBar, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  title: {
    flexGrow: 1,
  },
}));

const AppBarComponent: React.FC = () => {
  const classes = useStyles();
  const history = useHistory();
  const onLogout = () => {
    localStorage.clear();
    history.push('/login');
  };

  return (
    <AppBar style={{ backgroundColor: 'gray' }} position='static'>
      <Toolbar>
        <Typography variant='h6' className={classes.title}>
          Task Management
        </Typography>
        <Button onClick={onLogout} color='inherit'>
          Logout
        </Button>
      </Toolbar>
    </AppBar>
  );
};

export default AppBarComponent;
